
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	VarSub = 0;
}

void _CYCLIC ProgramCyclic(void)
{
	MsgTotal = ArPubSubDGetUdpMsgReceiveTotal();
	MsgError = ArPubSubDGetUdpMsgReceiveError();
}

void _EXIT ProgramExit(void)
{

}

