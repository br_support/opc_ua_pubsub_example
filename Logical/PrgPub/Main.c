
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
}

void _CYCLIC ProgramCyclic(void)
{
	VarPub++;
	MsgTotal = ArPubSubDGetUdpMsgSendTotal();
	MsgError = ArPubSubDGetUdpMsgSendError();
}

void _EXIT ProgramExit(void)
{

}

